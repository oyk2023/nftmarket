// SPDX-License-Identifier: APGL v3
pragma solidity 0.8;

import "hardhat/console.sol";


contract MarketPlace {


    struct Item {
        uint256 tokenId;
        uint256 price;
        bool solded;
        uint256 soldPrice;
        uint256 soldDate;
        bool onSale;
        uint256 listDate;
        address payable contractAddress;
        address owner;
    }

    string[] categories;
    
    mapping(string=>Item[]) categoriesToItems;
    mapping(string=>uint256) categoriesToItemLength;
    Item[] items;
    uint256 itemLength=0;



    function hasItem(string memory tag, uint256 tokenId,mapping(string=>Item[]) storage cats ) internal view returns(bool) {
        for(uint256 i=0;i<cats[tag].length;i++) {
            if (cats[tag][i].tokenId == tokenId) {
                return false;
            }
        }
        return true;
    }
    function categoriesList() external view returns(string[] memory) {
        return categories;
    }


    function categoriesToItemList(string memory tag,uint256 limit, uint256 offset) external view returns(Item[] memory) {
        Item[] memory catItems = categoriesToItems[tag];
        require(catItems.length >= offset, "offset is over length");
        uint256 lastitem = limit+offset;
        if (items.length < lastitem)  {
            lastitem = items.length;
        }
        Item[] memory tempItem;
        uint itemx  = 0;
        for (uint256 i=offset; i<lastitem;i++) {
            tempItem[itemx] = catItems[i];
            itemx++;
        }
        return tempItem;
    }

    function buyItem(uint256 tokenId, address payable contractAddress) external payable returns(bool) {


        Item memory item;
        for(uint256 i=0;i<items.length;i++) {
            if (items[i].contractAddress == contractAddress && items[i].tokenId == tokenId) {
                    require(items[i].onSale, "item is not sale");
                    item = items[i];
            }
        }
        require(payable(msg.sender).balance >= item.price, "your balance is low");

        //address payable thisAddress = payable(address(this));
        console.log(payable( msg.sender).balance);
        payable( msg.sender).transfer(item.price);
        //console.log(msg.value);
        address payable itemOwner = payable(item.owner);
        console.log(payable( msg.sender).balance);
        //itemOwner.transfer(item.price);
        //item.contractAddress.transferFrom(msg.sender)

        return true;
    }
    
    function allItems(uint256 limit, uint256 offset) external view returns(Item[] memory) {
        require(itemLength >= offset, "offset is over length");
        uint256 lastitem = limit+offset;
        if (itemLength < lastitem)  {
            lastitem = itemLength;
        }
        Item[] memory tempItem = new Item[](lastitem-offset);
        uint itemx  = 0;
        for (uint256 i=offset; i<lastitem;i++) {
            tempItem[itemx] = items[i];
            itemx++;
        }
        return tempItem;
    }

    function addItemToMarketPlace(uint256 tokenId, uint256 price, /*bytes32[] memory tags,*/address payable contractAddress) external returns (bool) {
        address owner  = msg.sender;
        Item memory item = Item(
            tokenId,
            price,
            false,
            0,
            0,
            true,
            block.timestamp,
            contractAddress,
            owner
        );
        items.push(item);
        itemLength++;
        /*
        for(uint256 i=0;i<tags.length;i++) {
            string memory tag = string(abi.encodePacked(tags[i]));
            if (hasItem(tag, tokenId, categoriesToItems)) {
                categoriesToItems[tag].push(item);
            }
        }*/

        return true;
    }

}