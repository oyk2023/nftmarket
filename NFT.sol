// SPDX-License-Identifier: APGL v3
pragma solidity 0.8;
import "https://raw.githubusercontent.com/OpenZeppelin/openzeppelin-contracts/master/contracts/token/ERC721/ERC721.sol";


contract MyToken is ERC721 {
    constructor() ERC721("MyToken", "MTK") {}
}
